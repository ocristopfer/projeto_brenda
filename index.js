const express = require('express');
const app = express();
const bodyParser = require('body-parser')

app.use(bodyParser())


app.post('/login', function(req, res){    
    const body = req.body;
    if(!body.username && !body.username === '') {
        res.status(422);
        res.send({message: 'projeto.brenda.login.username.erro', data: 'Nenhum usuário encontrado para o login'});
    }

    if(!body.password && !body.password === '') {
        res.status(422);
        res.send({message: 'projeto.brenda.login.password.erro', data: 'Nenhuma senha encontrada para o login'});
    }

    if(body.username.toLowerCase() === 'admin' && body.password === '123456') {
        res.status(200);
        res.send({message: 'projeto.brenda.login.password.sucesso', data: 'logado com sucesso.'});
    } else {
        res.status(500);
        res.send({message: 'projeto.brenda.login.erro', data: 'usuário e senha inválidos.'});
    } 
});

app.get('/listar', function(req, res) {
    const lista = [];

    lista.push({
        produto: 'arroz',
        peso: 15,
        cadastro: '15/03/2019',
        preço: '15.00',
        quantidade: 20
    });

    lista.push({
        produto: 'feijão',
        peso: 20,
        cadastro: '17/09/2019',
        preço: '10.00',
        quantidade: 30
    });

    lista.push({
        produto: 'ovo',
        peso: 2,
        cadastro: '19/04/2019',
        preço: '5.00',
        quantidade: 50
    });

    lista.push({
        produto: 'batata',
        peso: 5,
        cadastro: '15/07/2019',
        preço: '7.00',
        quantidade: 100
    });

    res.status(200);
    res.send(lista);

});
  
app.listen(3000, function () {
   console.log('Example app listening on port 3000!');
});