import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
        this.loginUrl = 'http://localhost:3000/listar';
        this.header = { 'Content-Type': 'application/json' };
    }
    LoginService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], LoginService);
    return LoginService;
}());
export { LoginService };
listar();
{
    this.http.get(this.loginUrl, { Headers: this.header }).toPromisse();
}
//# sourceMappingURL=login.service.js.map