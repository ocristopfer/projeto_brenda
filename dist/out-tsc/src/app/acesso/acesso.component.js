import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { LoginService } from '../login.service';
var AcessoComponent = /** @class */ (function () {
    function AcessoComponent(LoginService) {
        this.LoginService = LoginService;
    }
    AcessoComponent.prototype.ngOnInit = function () {
        this.listar();
    };
    AcessoComponent.prototype.listar = function () {
        //this.LoginService.listar().subscribe(res => this.login = res); 
    };
    AcessoComponent = tslib_1.__decorate([
        Component({
            selector: 'app-acesso',
            templateUrl: './acesso.component.html',
            styleUrls: ['./acesso.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService])
    ], AcessoComponent);
    return AcessoComponent;
}());
export { AcessoComponent };
//# sourceMappingURL=acesso.component.js.map