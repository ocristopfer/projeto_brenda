import { Component, OnInit } from '@angular/core';
import {ListarService} from '../listar.service'; 
import {HttpClient} from '@angular/common/http';
import {LIST_API} from '../app.api';

@Component({ 
  selector: 'app-acesso',
  templateUrl: './acesso.component.html',
  styleUrls: ['./acesso.component.scss']
})
export class AcessoComponent implements OnInit {
  
   
  constructor(private listarService: ListarService) { 
    console.log('contructor')
  }

  ngOnInit() {
    this.listarService.listar();
    console.log('logou !!!');

    
  }
}