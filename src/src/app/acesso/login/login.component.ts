import { Component, OnInit } from '@angular/core';
import { ListarService } from 'src/app/listar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
  

export class ListarComponent implements OnInit {

  public lista:any=[]; 
  
  constructor(private loginService : ListarService) {}//console.log(loginService.listar())
  
  
  ngOnInit() { }  
  
  //
  login(){
    this.loginService.listar().then(lista => {
      console.log('lista:', lista);
      this.lista = lista;
    });
   }

}



