import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LIST_API} from './app.api';

@Injectable()
export class ListarService {
  //loginUrl = 'http://localhost:3000/listar';
  header = {'Content-Type': 'application/json'}

constructor(private http: HttpClient) { 
  
}

    listar(){  
    
    return this.http.get(LIST_API, {headers: this.header}).toPromise();
  }
}