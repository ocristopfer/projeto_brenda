import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClient} from '@angular/common/http';
import {HttpClientModule} from '@angular/common/http';

import { ListarComponent } from './acesso/login/login.component';
import { AcessoComponent } from './acesso/acesso.component';
import { ListarService } from './listar.service';
import { LogarService } from './logar.service';
import { AutentComponent } from './acesso/autent/autent.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarComponent,
    AcessoComponent,
    AutentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ListarService, LogarService],
  bootstrap: [AppComponent]
})

export class AppModule { }
