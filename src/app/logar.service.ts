import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LIST_API} from './app.api';

@Injectable()
export class LogarService {
  loginUrl = 'http://localhost:3000/logar';
  header = {'Content-Type': 'application/json'}

constructor(private http: HttpClient) { 
  
}

sendCredential(username: string, password: string) {
    const url = 'http://localhost:8080/logar';
    //do not need to stringify your body
    const body = {
        username, password
    }
  
    return this.http.post(url, body, {headers: this.header});
}



/*    listarlog(){  
    return this.http.get(this.loginUrl, {headers: this.header}).toPromise();
  }*/
}